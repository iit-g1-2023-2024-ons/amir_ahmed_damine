package tn.iit.teamservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import tn.iit.teamservice.model.Team;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlayerRequest {
    private String firstName;
    private String lastName;
    private String nationality;
    private String position;
}
