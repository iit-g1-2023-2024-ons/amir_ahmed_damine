package tn.iit.teamservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tn.iit.teamservice.model.Team;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlayerResponse {
    private long id;
    private String firstName;
    private String lastName;
    private String nationality;
    private String position;
    private Team team;
}
