package tn.iit.teamservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import tn.iit.teamservice.model.Player;

public interface PlayerRepository extends JpaRepository<Player,Long> {
}
