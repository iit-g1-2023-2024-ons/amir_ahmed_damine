package tn.iit.teamservice.repository;

import jakarta.persistence.Tuple;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tn.iit.teamservice.dto.TeamResponse;
import tn.iit.teamservice.model.Team;

public interface TeamRepository extends JpaRepository<Team,Long> {

    @Query("SELECT t.id AS id, t.name AS name FROM Team t WHERE t.id = :id")
    Team findTeamByIdTuple(@Param("id") Long id);

}
