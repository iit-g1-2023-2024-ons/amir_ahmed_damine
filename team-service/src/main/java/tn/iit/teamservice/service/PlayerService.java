package tn.iit.teamservice.service;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tn.iit.teamservice.dto.PlayerRequest;
import tn.iit.teamservice.dto.PlayerResponse;
import tn.iit.teamservice.dto.TeamResponse;
import tn.iit.teamservice.model.Player;
import tn.iit.teamservice.model.Team;
import tn.iit.teamservice.repository.PlayerRepository;
import tn.iit.teamservice.repository.TeamRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PlayerService {

    private final PlayerRepository playerRepository;

    private final TeamRepository teamRepository;
    public String addPlayer(PlayerRequest playerRequest) {
        Player player = new Player();
        player.setFirstName(playerRequest.getFirstName());
        player.setLastName(playerRequest.getLastName());
        player.setPosition(playerRequest.getPosition());
        player.setNationality(playerRequest.getNationality());
        playerRepository.save(player);
        return "player created";
    }

    public List<PlayerResponse> getAllPlayers() {
        return playerRepository.findAll().stream().map(this::mapToPlayersResponse).toList();
    }

    private PlayerResponse mapToPlayersResponse(Player player) {
        return PlayerResponse.builder()
                .id(player.getId())
                .firstName(player.getFirstName())
                .lastName(player.getLastName())
                .nationality(player.getNationality())
                .position(player.getPosition())
                .team((player.getTeam()))
                .build();
    }

    public Optional<Player> getPlayerById(long id) {
        return playerRepository.findById(id);
    }

    public String updatePlayer(Long id, PlayerRequest playerRequest) {
        Player player = playerRepository.getReferenceById(id);
        player.setFirstName(playerRequest.getFirstName());
        player.setLastName(playerRequest.getLastName());
        player.setNationality(player.getNationality());
        player.setPosition(playerRequest.getPosition());
        playerRepository.save(player);
        return "team updated";
    }

    public void deletePlayer(Long id) {
        Player existingPlayer = playerRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Team not found with id: " + id));

        // Delete the team
        playerRepository.delete(existingPlayer);
    }

    public String addPlayertoATeam(Long id, Long teamId, PlayerRequest playerRequest) {
        Player player =playerRepository.getReferenceById(id);
        Team team = teamRepository.getReferenceById(teamId);
        player.setTeam(team);
        team.getTeamPlayers().add(player);
        playerRepository.save(player);
        teamRepository.save(team);
        return "player added to a team";
    }
}
