package tn.iit.teamservice.service;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tn.iit.teamservice.dto.TeamRequest;
import tn.iit.teamservice.dto.TeamResponse;
import tn.iit.teamservice.model.Team;
import tn.iit.teamservice.repository.TeamRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TeamService {
    private final TeamRepository teamRepository;
    public String addTeam(TeamRequest teamRequest) {
        try{
            Team team = new Team();
            team.setName(teamRequest.getName());
            team.setTrainer(teamRequest.getTrainer());
            teamRepository.save(team);
            return "team created";
        }catch (Exception e){
            return e.getMessage();
        }
    }


    public List<TeamResponse> getAllTeams() {
        return teamRepository.findAll().stream().map(this::mapToTeamsResponse).toList();
    }

    private TeamResponse mapToTeamsResponse(Team team) {
        return TeamResponse.builder()
                .id(team.getId())
                .name(team.getName())
                .trainer(team.getTrainer())
                .teamPlayers(team.getTeamPlayers())
                .build();
    }

    public Optional<Team> getTeamById(Long id) {
        return teamRepository.findById(id);
    }

    public String updateTeam(Long id, TeamRequest updatedTeamRequest) {
        Team team = teamRepository.getReferenceById(id);
        team.setName(updatedTeamRequest.getName());
        team.setTrainer(updatedTeamRequest.getTrainer());
        teamRepository.save(team);
        return "team updated";
    }

    public void deleteTeam(Long id) {
        // Check if the team exists
        Team existingTeam = teamRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Team not found with id: " + id));

        // Delete the team
        teamRepository.delete(existingTeam);
    }

}
