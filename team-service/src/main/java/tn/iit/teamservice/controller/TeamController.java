package tn.iit.teamservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.iit.teamservice.dto.TeamRequest;
import tn.iit.teamservice.dto.TeamResponse;
import tn.iit.teamservice.model.Team;
import tn.iit.teamservice.service.TeamService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/team")
@RequiredArgsConstructor
public class TeamController {

    private final TeamService teamService;
    @PostMapping
    public String addTeam(@RequestBody TeamRequest teamRequest){
        return teamService.addTeam(teamRequest);
    }

    @GetMapping
    public List<TeamResponse> getAllTeams() {
        return teamService.getAllTeams();
    }

    @GetMapping("/{id}")
    public Optional<Team> getTeamById(@PathVariable long id) {
        return teamService.getTeamById(id);
    }

    @PutMapping("/{id}")
    public String updateTeam(@PathVariable Long id, @RequestBody TeamRequest updatedTeamRequest) {
        return teamService.updateTeam(id, updatedTeamRequest);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteTeam(@PathVariable Long id) {
        teamService.deleteTeam(id);
        return ResponseEntity.ok("Team with id " + id + " deleted successfully");
    }




}
