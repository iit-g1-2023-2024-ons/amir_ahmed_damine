package tn.iit.teamservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.iit.teamservice.dto.PlayerRequest;
import tn.iit.teamservice.dto.PlayerResponse;
import tn.iit.teamservice.dto.TeamRequest;
import tn.iit.teamservice.dto.TeamResponse;
import tn.iit.teamservice.model.Player;
import tn.iit.teamservice.model.Team;
import tn.iit.teamservice.service.PlayerService;
import tn.iit.teamservice.service.TeamService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/player")
@RequiredArgsConstructor
public class PlayerController {

    private final PlayerService playerService;
    @PostMapping
    public String addPlayer(@RequestBody PlayerRequest playerRequest){
        return playerService.addPlayer(playerRequest);
    }

    @GetMapping
    public List<PlayerResponse> getAllPlayers() {
        return playerService.getAllPlayers();
    }

    @GetMapping("/{id}")
    public Optional<Player> getPlayerById(@PathVariable long id) {
        return playerService.getPlayerById(id);
    }

    @PutMapping("/{id}")
    public String updatePlayer(@PathVariable Long id, @RequestBody PlayerRequest playerRequest) {
        return playerService.updatePlayer(id, playerRequest);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePlayer(@PathVariable Long id) {
        playerService.deletePlayer(id);
        return ResponseEntity.ok("Team with id " + id + " deleted successfully");
    }

    @PutMapping("/{id}/teams/{team_id}")
    public String addPlayertoATeam(@PathVariable Long id, @PathVariable Long team_id, @RequestBody PlayerRequest playerRequest) {
        return playerService.addPlayertoATeam(id,team_id, playerRequest);
    }
}
