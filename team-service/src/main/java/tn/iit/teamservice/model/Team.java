package tn.iit.teamservice.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.netflix.discovery.provider.Serializer;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Serializer
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String trainer;
    @OneToMany(cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Player> teamPlayers;


}
