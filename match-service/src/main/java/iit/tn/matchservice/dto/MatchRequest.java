package iit.tn.matchservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MatchRequest {

    private long teamAId;
    private long teamBId;
    private String field;
    private long cap;
    private String arb;
    private LocalDate date;
    private long nbSpec;
}
