package iit.tn.matchservice.service;


import iit.tn.matchservice.dto.MatchRequest;
import iit.tn.matchservice.model.Match;
import iit.tn.matchservice.proxy.TeamServiceFeignClient;
import iit.tn.matchservice.repository.MatchRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tn.iit.teamservice.dto.TeamResponse;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MatchService {

    private final TeamServiceFeignClient teamServiceFeignClient;

    private final MatchRepository matchRepository;
    public String addMatch(MatchRequest matchRequest) {
        Match match = new Match();
        TeamResponse teamById = teamServiceFeignClient.getTeamById(matchRequest.getTeamAId());
        TeamResponse teamById1 = teamServiceFeignClient.getTeamById(matchRequest.getTeamBId());
        match.setTeamAId(teamById.getId());
        match.setTeamBId(teamById1.getId());
        match.setArb(matchRequest.getArb());
        match.setField(matchRequest.getField());
        match.setDate(matchRequest.getDate());
        match.setCap(matchRequest.getCap());
        match.setNbSpec(matchRequest.getNbSpec());
        matchRepository.save(match);

        return "Match added";
    }

    public List<Match> getAllMatches() {
        return matchRepository.findAll();
    }
}
