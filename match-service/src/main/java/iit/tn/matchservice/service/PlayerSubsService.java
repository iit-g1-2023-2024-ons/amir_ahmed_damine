package iit.tn.matchservice.service;

import iit.tn.matchservice.dto.PlayerSubsRequest;
import iit.tn.matchservice.model.Goal;
import iit.tn.matchservice.model.Match;
import iit.tn.matchservice.model.Player;
import iit.tn.matchservice.proxy.TeamServiceFeignClient;
import iit.tn.matchservice.repository.GoalRepository;
import iit.tn.matchservice.repository.MatchRepository;
import iit.tn.matchservice.repository.PlayerSubsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tn.iit.teamservice.dto.PlayerResponse;

@Service
@RequiredArgsConstructor
public class PlayerSubsService {
    private final TeamServiceFeignClient teamServiceFeignClient;
    private  final PlayerSubsRepository playerSubsRepository;
    private final MatchRepository matchRepository;

    public String addSubs(PlayerSubsRequest playerSubsRequest, long id) {
        PlayerResponse playerResponseIn=teamServiceFeignClient.getPlayerById(playerSubsRequest.getPlayerIdIn());
        PlayerResponse playerResponseOut=teamServiceFeignClient.getPlayerById(playerSubsRequest.getPlayerIdOut());
        Player player = new Player();
        Match match = matchRepository.getReferenceById(id);

        player.setPlayerIdIn(playerResponseIn.getId());
        player.setPlayerIdOut(playerResponseOut.getId());
        playerSubsRepository.save(player);
        match.getSubsList().add(player);
        matchRepository.save(match);
        return "subs added";
    }
}
