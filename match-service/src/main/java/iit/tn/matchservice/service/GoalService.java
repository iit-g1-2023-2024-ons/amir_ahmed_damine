package iit.tn.matchservice.service;

import iit.tn.matchservice.dto.GoalRequest;
import iit.tn.matchservice.model.Goal;
import iit.tn.matchservice.model.Match;
import iit.tn.matchservice.proxy.TeamServiceFeignClient;
import iit.tn.matchservice.repository.GoalRepository;
import iit.tn.matchservice.repository.MatchRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tn.iit.teamservice.dto.PlayerResponse;

@Service
@RequiredArgsConstructor
public class GoalService {

    private final TeamServiceFeignClient teamServiceFeignClient;
    private  final GoalRepository goalRepository;
    private final MatchRepository matchRepository;
    public String addGaol(GoalRequest goalRequest,Long id) {
        PlayerResponse playerResponse=teamServiceFeignClient.getPlayerById(goalRequest.getPlayerId());
        Goal goal = new Goal();
        Match match = matchRepository.getReferenceById(id);

        goal.setPlayerId(playerResponse.getId());
        goal.setMin(goalRequest.getMin());
        goalRepository.save(goal);
        match.getGoalList().add(goal);
        matchRepository.save(match);
        return "goal added";
    }
}
