package iit.tn.matchservice.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Table(name = "table_match")
public class Match {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id ;

    private long teamAId;
    private long teamBId;
    private String field;
    private long cap;
    private String arb;
    private LocalDate date;
    private long nbSpec;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Goal> goalList;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Player> subsList;


}
