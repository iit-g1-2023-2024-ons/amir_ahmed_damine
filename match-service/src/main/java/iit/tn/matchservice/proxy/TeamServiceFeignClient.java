package iit.tn.matchservice.proxy;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import tn.iit.teamservice.dto.PlayerResponse;
import tn.iit.teamservice.dto.TeamResponse;


@FeignClient(value = "api-gateway", path = "/team-service/api")
public interface TeamServiceFeignClient {

    @GetMapping("/team/{id}")
    TeamResponse getTeamById(@PathVariable Long id);
    @GetMapping("/player/{id}")
    PlayerResponse getPlayerById(@PathVariable Long id);
}
