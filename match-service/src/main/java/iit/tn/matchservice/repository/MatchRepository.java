package iit.tn.matchservice.repository;

import iit.tn.matchservice.model.Match;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MatchRepository extends JpaRepository<Match,Long> {
}
