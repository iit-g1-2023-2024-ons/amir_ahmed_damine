package iit.tn.matchservice.repository;

import iit.tn.matchservice.model.Goal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoalRepository extends JpaRepository<Goal,Long> {
}
