package iit.tn.matchservice.repository;

import iit.tn.matchservice.model.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayerSubsRepository extends JpaRepository<Player,Long> {
}
