package iit.tn.matchservice.controller;

import iit.tn.matchservice.dto.GoalRequest;
import iit.tn.matchservice.dto.PlayerSubsRequest;
import iit.tn.matchservice.service.GoalService;
import iit.tn.matchservice.service.PlayerSubsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/match/subs")
@RequiredArgsConstructor
public class PlayerSubsController {

    private final PlayerSubsService playerSubsService;
    @PostMapping("/{id}")
    public String addSubs(@RequestBody PlayerSubsRequest playerSubsRequest, @PathVariable long id){
        return playerSubsService.addSubs(playerSubsRequest,id);
    }
}
