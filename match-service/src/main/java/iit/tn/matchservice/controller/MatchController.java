package iit.tn.matchservice.controller;

import iit.tn.matchservice.dto.MatchRequest;
import iit.tn.matchservice.model.Match;
import iit.tn.matchservice.service.MatchService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/match")
@RequiredArgsConstructor
public class MatchController {

    private final MatchService matchService;
    @PostMapping
    public String addMatch(@RequestBody MatchRequest matchRequest){
        return matchService.addMatch(matchRequest);
    }

    @GetMapping
    public ResponseEntity<List<Match>> getAllMatches() {
        List<Match> matches = matchService.getAllMatches();
        return new ResponseEntity<>(matches, HttpStatus.OK);
    }
}
