package iit.tn.matchservice.controller;

import iit.tn.matchservice.dto.GoalRequest;
import iit.tn.matchservice.dto.MatchRequest;
import iit.tn.matchservice.service.GoalService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/match")
@RequiredArgsConstructor
public class GoalController {

    private final GoalService goalService;
    @PostMapping("/{id}")
    public String addMatch(@RequestBody GoalRequest goalRequest, @PathVariable long id){
        return goalService.addGaol(goalRequest,id);
    }
}
