package tn.iit.eurekaservise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServiseApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaServiseApplication.class, args);
    }

}
